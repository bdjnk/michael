const input = document.getElementById('search');
const results = document.getElementById('results');

const index = elasticlunr.Index.load(window.searchIndex);

let prevTerm = '';

function doSearch(event) {
  if (event && event.key === 'Escape') {
    if (input.value) {
      event.stopPropagation();
    }
    input.value = '';
  }
  const term = input.value.trim();

  if (!index || prevTerm === term) return;
  prevTerm = term;

  while (results.firstChild) results.firstChild.remove();

  if (term.length < 2) return;

  const options = {
    expand: true,
    bool: 'AND',
    fields: {
      title: { boost: 2 },
      body: { boost: 1 }
    }
  };

  const hits = index.search(term, options);
  if (!hits.length) return;

  const dom = hits.reduce((fragment, hit) => {
    const a = document.createElement('a');
    a.href = hit.ref;
    a.textContent = hit.doc.title;

    const li = document.createElement('li');
    li.appendChild(a);
    fragment.appendChild(li);
    return fragment;
  }, document.createDocumentFragment());

  results.appendChild(dom);
}

if (input) {
  input.focus();
  doSearch();
  input.addEventListener('focus', doSearch);
  input.addEventListener('keyup', doSearch);
  input.addEventListener('change', doSearch);
  input.addEventListener('blur', doSearch);

  document.addEventListener('keyup', event => {
    if (event && event.key === 'Escape') {
      if (!input.value) {
        window.history.back();
      }
    }
  });
}
