window.addEventListener("pagehide", event => {
  document.activeElement.blur();
});

// QUOTES

Array.from(document.getElementsByTagName('blockquote')).forEach(blockquote => {
  blockquote.insertAdjacentHTML('afterbegin', '<i class="bx bxs-quote-right bx-flip-horizontal"></i>');
  blockquote.insertAdjacentHTML('beforeend', '<i class="bx bxs-quote-right"></i>');
});

// NAV DISABLE

const noPointer = id => {
    document.getElementById(id).style.setProperty('pointer-events', 'none');
}

switch (location.pathname) {
  case '/':
    noPointer('nav-home');
    break;
  case '/search':
    noPointer('nav-search');
    break;
  case '/tag':
    noPointer('nav-tag');
    break;
}

// TABLE OF CONTENTS

const tocToggle = document.getElementById('toc-toggle');
const tocMenu = document.getElementById('toc-menu');

if (tocToggle && tocMenu) {

  tocToggle.addEventListener('click', event => {
    if (tocMenu.classList.toggle('gone')) {
      tocToggle.blur();
    }
    event.stopPropagation();
  });

  window.addEventListener('click', event => {
    if (!tocMenu.contains(event.target) || event.target.tagName === 'A') {
      tocMenu.classList.add('gone');
    } else {
      tocToggle.focus();
    }
  });
}

// JUMP TO TOP BUTTON

const toTop = document.getElementById('to-top');

if (toTop) {

  function setToTopVisibility() {
    if (document.documentElement.scrollTop > 90) {
      toTop.classList.remove('hid');
    } else {
      toTop.classList.add('hid');
    }
  }

  setToTopVisibility();

  toTop.addEventListener('click', event => {
    toTop.blur();
  });

  window.addEventListener('scroll', event => {
    setToTopVisibility();
  });
}

// GET SCROLLBAR WIDTH

function getScrollbarWidth() {
  const outer = document.createElement('div');
  document.body.appendChild(outer);

  outer.style.visibility = 'hidden';
  outer.style.overflow = 'scroll';

  const inner = document.createElement('div');
  outer.appendChild(inner);

  const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);

  outer.parentNode.removeChild(outer);

  return scrollbarWidth;
}

document.documentElement.style.setProperty('--scrollbar-width', `${getScrollbarWidth()}px`);
