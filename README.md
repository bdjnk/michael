A personal website, built with [zola](https://www.getzola.org/), available at [michael.plotke.me](https://michael.plotke.me/).
