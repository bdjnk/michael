+++
title = "Endorsements"
template = "portfolio.html"
+++

Among the many wonderful, skilled engineers I've had the priviledge to work alongside, some few I've asked to endorse me. They have been generous with their words of priase and I cannot adequently express how grateful I am. To them I say, thank you, a thousand times thank you.

## Mateo Williford

Mateo was the technical leader of the integrations team at Poll Everywhere, and my immediate superior. We worked together frequently through my year there. He graciously wrote me this endorsement on November 6th 2023, within a week of my having been laid off.

> Michael is one of the best engineers and most intelligent people I have ever had the privilege to work with over the course of my career. My weekly 1:1s with Michael were typically me asking for his guidance on some deep question involving architecture, team processes, or planning a path forward on a complex technical migration. 
>
> Michael has a propensity for quickly getting to the root of any problem, and challenging prior assumptions that others might not be aware of. As an example of this: When tasked with a project updating one of our core dependencies with the objective of resolving some customer facing issues, Michael determined that no version of this dependency would suit our needs, did research on various alternatives, and then successfully planned the technical migration and led the team on its implementation and delivery. This resulted in not only resolving the issues that we were attempting to resolve initially, but also fixed the majority of open bugs in this application with a single release.
>
> I could go on - Michael researched and implemented a mechanism for robust automated testing of our native Windows application, something our team has wanted for years but weren't able to make progress on until Michael came to help. Michael designed our company's process for technical research and prototyping which is now accelerating work across all of our engineering teams. Michael spent much of his time mentoring our engineers to gain a more holistic understanding of our software architecture which has enabled them to reach for more senior positions at the company.
>
> I only got to work with Michael for a single year, but I'm not exaggerating when I state that the majority of what our team accomplished over that year just wouldn't have happened without Michael. Michael took hard things we had been struggling with, and made them simple and actionable. Michael will have a profound positive impact on any team he works with; I know we learned a lot from him from his time at Poll Everywhere

## Teyler Halama

Teyler was a fellow senior software engineer at Poll Everywhere on the same team. We worked closely together through my year there. He graciously wrote me this endorsement on November 2nd 2023, the day after I was laid off.

> Michael is brilliant to the core. Recently while working with Michael, he lead the effort to simplify our organizations most utilized and complicated native app. His pitch to replace a core rendering engine lead to our team solving 10+ years worth of tech debt, bugs, and performance issues. We were convinced that all of those issues and bugs were unsolvable before Michael was a part of our company.
>
> Pairing / working directly with Michael day to day is also a dream. He regularly refers directly to the docs when questioning what a third party method does. He ensures every line of code makes sense, is readable, and optimized for future maintainability. Michael also cares very deeply about the people he works with. Talking with him while something was building or we had a tiny bit of downtime was fantastic and interesting. He does all of this while being a natural leader. He will never hesitate to answer a question during paring and is keenly interested in making sure everyone is up to speed so the team can perform to its fullest potential; not just himself.
>
> Michael is a truly brilliant, hard working, selfless, and will make a profound positive impact on any team that he is a part of.

## Hady Mohamed

Hady was a junior software engineer at Poll Everywhere on the same team. We occasionally worked together through my year there. He graciously wrote me this endorsement of November 2nd 2023, the day after I was laid off.

> I am delighted to write this recommendation for Michael, a colleague with whom I have had the privilege of collaborating closely since I joined Poll Everywhere a few months ago. As a newcomer to both the company and the software engineering profession, I was fortunate to find myself on the same team as Michael, whose guidance and support have been instrumental in my professional development. Michael is a standout in the field of Software Architecture, demonstrating a masterful command of C# and Ruby on Rails. His ability to architect complex systems and craft elegant solutions is matched only by his meticulous attention to detail and deep understanding of best practices in software development.
>
> One of Michael's most commendable qualities is his problem-solving prowess. He approaches challenges with a strategic mindset, carefully weighing both immediate and future implications to devise solutions that are not only effective but also sustainable. His contributions have been vital in optimizing our Software Project Management processes, driving efficiency, and elevating the quality of our work. What sets Michael apart is his extraordinary communication skills and his willingness to collaborate. Despite his extensive knowledge and experience, he remains humble and approachable. He has a rare talent for breaking down complex technical concepts into easily digestible terms, making knowledge transfer seamless and fostering a collaborative team environment.
>
> As a junior member of the team, I have benefited immensely from Michael's mentorship. He has always been generous with his time, patiently explaining concepts and working alongside me to solve challenging problems. His dedication to helping others and his unwavering support have not only accelerated my learning curve but have also significantly contributed to the team's success. I am confident that Michael will excel in any endeavor he undertakes. His blend of technical expertise, innovative thinking, and collaborative spirit is exceptional. Any organization would be fortunate to have Michael as a part of their team, as he is not only a brilliant software engineer but also a true team player who consistently goes above and beyond.

Although Hady says I demostrated a masterful command of C# and Ruby of Rails, this is an overly generous assessment of my knowledge. I have enough experience across various languages and frameworks to quickly present as masterful to more novice developers. This is a worthwhile skill, but it isn't instant mastery.

## Henrik Volckmer

 Henrik was a senior software engineer at MedForce while I was a more junior engineer. We worked together frequently though my year there. He graciously wrote me this endorsement on July 19 2016, shortly before I departed the company.

> Michael is going to build great software wherever he goes. With his commitment to detail and ability to learn and adapt quickly, Michael has managed to produce working web applications under difficult circumstances and nuanced requirements during the year I've worked with him. Michael will be a great asset to any team.
