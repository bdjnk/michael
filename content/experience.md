+++
date = "2023-12-05T17:51:04Z"
title = "Story of Leadership and Impact"
template = "portfolio.html"
+++

I was asked recently to produce a presentation covering *a significant team effort* and *a short term, high impact experience* from my professional life. This is the content of that presentation, formatted as a story. 

## A significant team effort.

Poll Everywhere has a PowerPoint add-in, built with C# and .NET Framework, from which we would initiate a separate Electron application process which provided slide overlays showing real-time survey results during presentations, and provided a modal for interacting with our web application, inserting surveys and more. Of pressing concern, our Electron version had significant open CVEs, but two prior attempts to update had manifested seemingly unsolvable DPI scaling issues. Furthermore, given that Electron updates rapidly and incautiously, such a situation, or one equally vexing, was likely to arise again.

I was aware of Tauri, an Electron alternative, which uses the native web view of each operating system, so I knew Windows must have a native web view, which would presumably be accessible from, and interoperate nicely with, our Windows application.

 Individual research confirmed WebView2 was exactly that, I drafted a high level proposal and pitched it to engineering and to product. This project would require broad buy-in. It would be more work than updating Electron, but not only would it fix the CVEs and improve DPI handling, it would also resolve a slew of longstanding bugs and decrease resource usage. We received approval for a research project to validate the proposal.

I worked with the new product manager to craft a detailed project document, and with my technical leader to develop a new research process. I proposed research be done in pairs with regular role swapping.

Two weeks we spent understanding all the uses of Electron within our application and how precisely Webview2 might be used to replace that functionality. Two weeks we spent implementing a working prototype of core functionality within the application. One week we spent ensuring peripheral functionality would work. One week we spent wrapping up details and finalizing documentation.

No consideration was given to architecture during prototyping, so this was our first task during implementation. How could we structure the new WebView2 code to be comprehensible, extensible, and testable?

We each proposed an architecture to meet these criteria and the final design was the result of discussion and agreement on the best arrangement.

While pulling code from the prototype into our new architecture we encountered a problem with environment sharing between the overlay and the modal. We filed a bug report with Microsoft, but we needed an immediate workaround. We tried multiple approaches, before settling on cookie syncronization as the only viable solution.

Thorough QA testing indicated we were ready to release a week early. We cut a release, tested it, and discovered it was fundamentally broken. It should have been the same, but there was something different between what QA was testing and the result of the release build process.

The team spent two days, first trying unsuccessfully to get the issue to reproduce in development, then trying to debug the production build using log files. Ultimately we discovered a JSON package we had updated in one module was, in production only, being optimized to share the lower version found in the other modules.

We updated that package everywhere, retested, and released. The resulting application was smaller, faster, more stable and secure, and delivered on time.

Further, we had a new research process we were pleased with, and we began moving to promote it to the rest of engineering.

Each of us involved grew and were bettered though this experience. I became more aware of the existence of exceptions to the tension between engineering excellence and business urgency. Indeed, these junctures of engineering and business needs are where the richest value is found.

At the same time, I developed confidence in leadership, to allow and encourage the contributions and growth of others, while directing progress through proposals and final decisions aimed at getting the best outcomes for everyone concerned.

## A short-term, high-impact experience.

Much of the C# and .NET Framework codebase for the Poll Everywhere PowerPoint add-in has unit test coverage. What it needed, in my estimation, was end-to-end testing.

This need came from the PowerPoint add-in’s reliance on the web application’s APIs and UIs, and the shared underlying code. The PowerPoint add-in has a lengthy update cycle, while the web application is updated in production multiple times every day. Any change to the web application could have breaking consequences for the PowerPoint add-in. Without an automated end-to-end test suite, testing would need to be done manually and Poll Everywhere did not have the resources to regression test multiple times a day.

Observing this problem, I asked my technical leader if I could prove a solution would it be prioritized. The answer was absolutely, but that it had been attempted repeatedly, and every attempt had ended in failure.

Undaunted, I reacquainted myself with AutoHotkey, which I had used years ago to build questionable tools to automate game play. In the course of investigating the current AutoHotkey ecosystem I found UIA-v2. This is a thin wrapper exposing Microsoft UI Automation to AutoHotkey, allowing any widget of any window to be programmatically controlled.

Impressed by the capabilities, I wrote a script to automate the naive case of the primary workflow. The morning after I got it running smoothly I presented the script live at the weekly all engineering meeting to significant appreciation.

A project was hastily arranged to develop a test suite, and within a week and a half that project began, at the same time as the WebView2 implementation project. By the time we were testing WebView2, while QA was doing it manually, we were running the new AutoHotkey test suite, catching real bugs that might otherwise have been released to production.

From all this I learned, if you want to propose a solution and you want it taken seriously, you need to show something real. It is not enough to have an idea, particularly in an environment where ideas are common and commonly abandoned. Instead you need working proof of viability.
