+++
date = 2025-03-03T10:02:04-06:00
title = "The Process of Crafting Excellent Code"

[taxonomies]
tag = ["coding"]
+++

Regardless of what you are coding, the steps in this process will help increase its quality.

If you feel stuck at any point, reach out and work with colleagues or mentors.

## Working Code

To begin, implement the primary functionality. Pay minimal attention to code quality.

A new coder will often find one hundred percent of this code will need to be rewritten. Even a veteran coder will ultimately rewrite, depending on the complexity of the task, a large percentage of this code.

As soon as the code works, stop immediately.

## Architectural Restructure

Examine the current architecture of your working solution. This is almost certainly not the best architecture.

Contemplate alternative architectural options. Pay attention to minimizing unnecessary indirection, matching established architectural patterns in the codebase, and generating reusable systems and components when you can't reuse existing ones.

Consider the architecture's performance and readability implications. If the data structures are not optimal, slow and convoluted code may be required downstream. You may need to work with a data provider to receive it in a preferable form.

Rewrite the architecture. Examine your new approach. If it doesn't delight you, repeat this restructure step until it does.

## Interface Quality

Turn your attention now the interfaces your code provides. These are the names of public variables, the inputs and outputs of public functions, and even the user interface, if applicable.

These interfaces should be consistent with other similar interfaces in the codebase, and should be as clear and intuitive as possible. Any unambiguous interface should be well documented describing its purpose and function.

## Internal Refactor

Now focus on the code within your functions and components, refactoring for readability, simplicity, and performance. Generally readability is the most important of these.

Honest and concise naming should be of particular concern. Cater in naming to a hypothetical future reader, new to this code, striving not to misled him. The particulars of naming are beyond the scope of this guide.

Though least important in an internal refactor, even here attempt to mimic the existing style of the codebase.

## Final Review

Peruse your code and ensure it pleases you. Now test it thoroughly, running through every scenario you can anticipate. Fix any bugs you uncover.

After this last solo action present your code to colleagues and mentors for review. Detail the reasons for the architectural choice you committed to. Hopefully the feedback will be minimal.