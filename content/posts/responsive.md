+++
date = 2018-09-14T08:14:08-04:00
title = "Designing and Coding a Responsive Website"

draft = true

[taxonomies]
tag = ["coding", "web"]
+++

From the requisite fundamentals to esoteric extras, this post walks through designing and coding a basic responsive website, like the one you're currently reading.

## Aforementioned Requisite Fundamentals

### Viewport Meta Tag

The viewport meta tag is (idk). The one for this website is as follows.

```html
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
```

### Media Queries

While there are several [media types](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries#Media_types) we'll be targeting `all`, and while there are many [media features](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries#Media_features) we'll be reacting to `width`.

## Promised Esoteric Extras

### Viewport-Percentage Lengths

While there are several [viewport-percentage lengths](https://developer.mozilla.org/en-US/docs/Web/CSS/length#Viewport-percentage_lengths), and they're all useful, we'll need `vw` (viewport width). A `vw` is one percent of the width of the view, so `100vw` is the width of the entire top level view.

```css
font-size: calc(0.83vw + 12px);
```
