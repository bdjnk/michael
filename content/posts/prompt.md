+++
date = 2012-11-19T13:32:35-04:00
title = "Using ANSI Color Codes to Colorize Your Bash Prompt on Linux"

[taxonomies]
tag = ["bash", "linux"]

[extra]
ansi = true
+++

<pre class="ansi2html-content">╔╣ <span class="ansi1 ansi32">*</span> ║ <span class="ansi36">08:24 AM Fri Aug 17</span> ║ <span class="ansi35">0</span> ║ michaelplotke<span class="ansi33">@work</span> ║ <span class="ansi1 ansi34">~/Desktop</span> ║ <span class="ansi32">11 files</span> ╠═╗
╚═<span class="ansi1 ansi34">»</span>
</pre>

The goal is to create a design similar to the above example, but with more artistic merit. To accomplish this, we must understand several things, including: special characters, some bash scripting, and ANSI colors.

## Box-Drawing Characters

Unicode provides an extensive range of symbols and special characters we can use to make our Bash prompt awesome. In my case, I am using the special [box drawing characters](https://en.wikipedia.org/wiki/Box-drawing_characters) to add cutesy little arrows and double lines.

## The Bash PS1 Variable

All of the content and the appearance of the Bash prompt is managed by the `PS1` variable, which is defined in `~/.bashrc` for each user, and in `/etc/*bashrc` for users without their own definition.

While the `PS1` variable can become incredibly complex, it can also be very simple. Here is the default that came with my Arch Linux install:

`PS1='[\u@\h \W]$ '`

This creates a simple prompt which displays:

`[michaelplotke@work Desktop]$`

Let's discuss how that works.

### Basic PS1 Codes

These are generally called Bash prompt "escape sequences", as they consist of a backslash (the standard escape character) followed by a regular character. Although there are [many available](http://tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html), I will just list the one's I've used:

`u` - the current user's username  
`h` - the current machine's hostname  
`j` - the number of jobs managed by this shell  
`@` - the current time (12 hour format)  
`d` - the current date  
`w` - path of the current working directory  
`W` - just the current working directory  
`e` - an ASCII escape character (033)  
`n` - adds a newline character  

### Summary

We can now clearly see how `'[\u@\h \W]$ '` becomes `[michaelplotke@work Desktop]$`, because it makes use of some basic PS1 codes and some plain text.

## Checking State

There are two states I want to track. The exit status of the previously run command, and whether or not I'm logged in as root. In the example above the PS1 is aware of these circumstances. Here's how:

### Exit Status

In Linux, an exit code of anything other than 0 generally indicates some kind of error. To get the exit code, we need to take a look at the `$?` variable. Try running various commands, then running `echo $?` right after from the same prompt. This should be 0 if the command completed successfully, and something other than 0 if it didn't.

For our purposes, we need to test the exit status. We can use the form `if [ $? -eq 0 ]; then echo 'good'; else echo 'bad'; fi` replacing the echo commands with whatever we want to do in each case.

### Root Prompt

In Linux, the root user receives the user id 0. The current user id for a given prompt is stored in the variable `$EUID`. Thus we test for root with `if [ ${EUID} -eq 0 ]; then echo 'root'; else echo 'user'; fi`.

## Gathering Data

Linux is blessed with a plethora of fast little command line utilities, perfect for gathering data to use in your Bash prompt. While I'm only getting the number of files, there are many other options, for example git information.

The [PATH variable](http://linuxconfig.org/linux-path-environment-variable) should be available when the prompt is contructed, so you should be able to use command exactly as you would in the terminal.

### Number of Files

I first use `ls -1` to generate a list of all the files (and folders, which are also called files in Linux) in the current directory. I then use `|` to pipe (pass) that data to `wc -l` which counts the number of newlines. This provides the number of files in the current directory.

The complete command is `ls -1 | wc -l`

## ANSI Colors

There are 16 [ANSI Colors](https://en.wikipedia.org/wiki/ANSI_escape_code#Colors), which are actually 8 colors, each having "normal" and "bright" intensity variants. The colors are black, red, green, yellow, blue, magenta, cyan, and white. In certain circumstances bright intensity could be actually brighter, or, in the case of an [xterm](http://en.wikipedia.org/wiki/Xterm), it could be bold.

<pre class="ansi2html-content">
                 40m     41m     42m     43m     44m     45m     46m     47m
     m   gYw   <span class="ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi47">  gYw </span><span class="ansi47"> </span>
    1m <span class="ansi1">  gYw   </span><span class="ansi1"></span><span class="ansi1 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1"></span><span class="ansi1 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1"></span><span class="ansi1 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1"></span><span class="ansi1 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1"></span><span class="ansi1 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1"></span><span class="ansi1 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1"></span><span class="ansi1 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1"></span><span class="ansi1 ansi47">  gYw </span><span class="ansi47"> </span>
   30m <span class="ansi30">  gYw   </span><span class="ansi30"></span><span class="ansi30 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi30"></span><span class="ansi30 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi30"></span><span class="ansi30 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi30"></span><span class="ansi30 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi30"></span><span class="ansi30 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi30"></span><span class="ansi30 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi30"></span><span class="ansi30 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi30"></span><span class="ansi30 ansi47">  gYw </span><span class="ansi47"> </span>
 1;30m <span class="ansi1 ansi30">  gYw   </span><span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi30"></span><span class="ansi1 ansi30 ansi47">  gYw </span><span class="ansi47"> </span>
   31m <span class="ansi31">  gYw   </span><span class="ansi31"></span><span class="ansi31 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi31"></span><span class="ansi31 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi31"></span><span class="ansi31 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi31"></span><span class="ansi31 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi31"></span><span class="ansi31 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi31"></span><span class="ansi31 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi31"></span><span class="ansi31 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi31"></span><span class="ansi31 ansi47">  gYw </span><span class="ansi47"> </span>
 1;31m <span class="ansi1 ansi31">  gYw   </span><span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi31"></span><span class="ansi1 ansi31 ansi47">  gYw </span><span class="ansi47"> </span>
   32m <span class="ansi32">  gYw   </span><span class="ansi32"></span><span class="ansi32 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi32"></span><span class="ansi32 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi32"></span><span class="ansi32 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi32"></span><span class="ansi32 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi32"></span><span class="ansi32 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi32"></span><span class="ansi32 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi32"></span><span class="ansi32 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi32"></span><span class="ansi32 ansi47">  gYw </span><span class="ansi47"> </span>
 1;32m <span class="ansi1 ansi32">  gYw   </span><span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi32"></span><span class="ansi1 ansi32 ansi47">  gYw </span><span class="ansi47"> </span>
   33m <span class="ansi33">  gYw   </span><span class="ansi33"></span><span class="ansi33 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi33"></span><span class="ansi33 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi33"></span><span class="ansi33 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi33"></span><span class="ansi33 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi33"></span><span class="ansi33 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi33"></span><span class="ansi33 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi33"></span><span class="ansi33 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi33"></span><span class="ansi33 ansi47">  gYw </span><span class="ansi47"> </span>
 1;33m <span class="ansi1 ansi33">  gYw   </span><span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi33"></span><span class="ansi1 ansi33 ansi47">  gYw </span><span class="ansi47"> </span>
   34m <span class="ansi34">  gYw   </span><span class="ansi34"></span><span class="ansi34 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi34"></span><span class="ansi34 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi34"></span><span class="ansi34 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi34"></span><span class="ansi34 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi34"></span><span class="ansi34 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi34"></span><span class="ansi34 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi34"></span><span class="ansi34 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi34"></span><span class="ansi34 ansi47">  gYw </span><span class="ansi47"> </span>
 1;34m <span class="ansi1 ansi34">  gYw   </span><span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi34"></span><span class="ansi1 ansi34 ansi47">  gYw </span><span class="ansi47"> </span>
   35m <span class="ansi35">  gYw   </span><span class="ansi35"></span><span class="ansi35 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi35"></span><span class="ansi35 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi35"></span><span class="ansi35 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi35"></span><span class="ansi35 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi35"></span><span class="ansi35 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi35"></span><span class="ansi35 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi35"></span><span class="ansi35 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi35"></span><span class="ansi35 ansi47">  gYw </span><span class="ansi47"> </span>
 1;35m <span class="ansi1 ansi35">  gYw   </span><span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi35"></span><span class="ansi1 ansi35 ansi47">  gYw </span><span class="ansi47"> </span>
   36m <span class="ansi36">  gYw   </span><span class="ansi36"></span><span class="ansi36 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi36"></span><span class="ansi36 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi36"></span><span class="ansi36 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi36"></span><span class="ansi36 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi36"></span><span class="ansi36 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi36"></span><span class="ansi36 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi36"></span><span class="ansi36 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi36"></span><span class="ansi36 ansi47">  gYw </span><span class="ansi47"> </span>
 1;36m <span class="ansi1 ansi36">  gYw   </span><span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi36"></span><span class="ansi1 ansi36 ansi47">  gYw </span><span class="ansi47"> </span>
   37m <span class="ansi37">  gYw   </span><span class="ansi37"></span><span class="ansi37 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi37"></span><span class="ansi37 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi37"></span><span class="ansi37 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi37"></span><span class="ansi37 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi37"></span><span class="ansi37 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi37"></span><span class="ansi37 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi37"></span><span class="ansi37 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi37"></span><span class="ansi37 ansi47">  gYw </span><span class="ansi47"> </span>
 1;37m <span class="ansi1 ansi37">  gYw   </span><span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi40">  gYw </span><span class="ansi40"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi41">  gYw </span><span class="ansi41"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi42">  gYw </span><span class="ansi42"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi43">  gYw </span><span class="ansi43"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi44">  gYw </span><span class="ansi44"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi45">  gYw </span><span class="ansi45"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi46">  gYw </span><span class="ansi46"> </span> <span class="ansi1 ansi37"></span><span class="ansi1 ansi37 ansi47">  gYw </span><span class="ansi47"> </span>
</pre>

### Usage Syntax

To set a font to the color and style desired, we use the syntax `\033[#m` where `#` can be a valid set of semicolon separated numbers. The `\033[` is a control sequence initiator which begins an escape sequence. In our case the sequence is a set of "`m`" terminated select graphic rendition parameters for rendering the text as desired. [More details](https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_codes).

If that makes no sense, just concentrate on which numbers to use. Font color makes use of 30 to 37. Background color makes use of 40 to 47. A 0 (or no value given) for the font color resets it to the default, which depends on the setup. For additional rendering options, 1 sets the font to bright / bold, 4 sets it to underlined, 7 sets it to negative colors.

It might help to test this by running this command.

```bash
echo -e "testing \033[0;37;41mCOLOR1\033[1;35;44mCOLOR2\033[m"
```

This should output `testing COLOR1COLOR2` with `COLOR1` being white (light gray) text on a red background, and `COLOR2` being bold / bright magenta text on a blue background. All subsequent text should be normally colored.

Try playing around with that echo command until you feel a bit more comfortable.

### Reference Table

Obviously you don't want to come back to this page every time you want to change your Bash colors. Here is the shell script ([origin](http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html)) used to generate the lovely color table:

```bash
#!/bin/bash
#
# This file echoes a bunch of color codes to the terminal to demonstrate
# what's available. Each line is the color code of one forground color,
# out of 17 (default + 16 escapes), followed by a test use of that color
# on all nine background colors (default + 8 escapes).
#
T='gYw'   # The test text
echo -e "\n                 40m     41m     42m     43m     44m     45m     46m     47m";
for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' '  36m' '1;36m' '  37m' '1;37m';
  do FG=${FGs// /}
  echo -en " $FGs \033[$FG  $T  "
  for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
    do echo -en "$EINS \033[$FG\033[$BG  $T \033[0m\033[$BG \033[0m";
  done
  echo;
done
echo
```

## Finally, the Goal

This is the PS1 which generates the slightly steam-punk looking Bash prompt shown at the start:

```bash
c="\[\033["
n="${c}m\]"
PS1="$n\n╔╣ "\
"${c}1;3\$(if [ \$? -eq 0 ]; then echo '2'; else echo '1'; fi)m\]*$n ║ "\
"${c}36m\]\@ \d$n ║ "\
"${c}35m\]\j$n ║ "\
"${c}30m\]\u"\
"${c}33m\]@\h$n ║ "\
"${c}1;34m\]\w$n ║ "\
"${c}32m\]\$(ls -1 | wc -l | tr -d ' ') files$n ╠═╗\n╚═"\
"${c}1;3\$(if [ ${EUID} -eq 0 ]; then echo '1'; else echo '4'; fi)m\]»$n "
```

Hopefully, at this point you are reading these lines of seeming gibberish like plain English. If not, play more. You'll get it.

## 256 (8-bit) Colors

It is also possible (in these newfangled modern times) to use a full 256 colors in most consoles. The following color table can be used to determine the code for each color by adding the column and row number. In order to make use of these codes, we use the syntax `33[38;5;#m` for the foreground (text) and `33[48;5;#m` for the background, or in a single statement like `33[38;5;#;48;5;#m` to set both at once, where `#` is an 8-bit (0-255) color code.

<pre class="ansi2html-content">
   +   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35

   0  <span class="ansi48-0">  </span> <span class="ansi48-1">  </span> <span class="ansi48-2">  </span> <span class="ansi48-3">  </span> <span class="ansi48-4">  </span> <span class="ansi48-5">  </span> <span class="ansi48-6">  </span> <span class="ansi48-7">  </span> <span class="ansi48-8">  </span> <span class="ansi48-9">  </span> <span class="ansi48-10">  </span> <span class="ansi48-11">  </span> <span class="ansi48-12">  </span> <span class="ansi48-13">  </span> <span class="ansi48-14">  </span> <span class="ansi48-15">  </span>

  16  <span class="ansi48-16">  </span> <span class="ansi48-17">  </span> <span class="ansi48-18">  </span> <span class="ansi48-19">  </span> <span class="ansi48-20">  </span> <span class="ansi48-21">  </span> <span class="ansi48-22">  </span> <span class="ansi48-23">  </span> <span class="ansi48-24">  </span> <span class="ansi48-25">  </span> <span class="ansi48-26">  </span> <span class="ansi48-27">  </span> <span class="ansi48-28">  </span> <span class="ansi48-29">  </span> <span class="ansi48-30">  </span> <span class="ansi48-31">  </span> <span class="ansi48-32">  </span> <span class="ansi48-33">  </span> <span class="ansi48-34">  </span> <span class="ansi48-35">  </span> <span class="ansi48-36">  </span> <span class="ansi48-37">  </span> <span class="ansi48-38">  </span> <span class="ansi48-39">  </span> <span class="ansi48-40">  </span> <span class="ansi48-41">  </span> <span class="ansi48-42">  </span> <span class="ansi48-43">  </span> <span class="ansi48-44">  </span> <span class="ansi48-45">  </span> <span class="ansi48-46">  </span> <span class="ansi48-47">  </span> <span class="ansi48-48">  </span> <span class="ansi48-49">  </span> <span class="ansi48-50">  </span> <span class="ansi48-51">  </span>

  52  <span class="ansi48-52">  </span> <span class="ansi48-53">  </span> <span class="ansi48-54">  </span> <span class="ansi48-55">  </span> <span class="ansi48-56">  </span> <span class="ansi48-57">  </span> <span class="ansi48-58">  </span> <span class="ansi48-59">  </span> <span class="ansi48-60">  </span> <span class="ansi48-61">  </span> <span class="ansi48-62">  </span> <span class="ansi48-63">  </span> <span class="ansi48-64">  </span> <span class="ansi48-65">  </span> <span class="ansi48-66">  </span> <span class="ansi48-67">  </span> <span class="ansi48-68">  </span> <span class="ansi48-69">  </span> <span class="ansi48-70">  </span> <span class="ansi48-71">  </span> <span class="ansi48-72">  </span> <span class="ansi48-73">  </span> <span class="ansi48-74">  </span> <span class="ansi48-75">  </span> <span class="ansi48-76">  </span> <span class="ansi48-77">  </span> <span class="ansi48-78">  </span> <span class="ansi48-79">  </span> <span class="ansi48-80">  </span> <span class="ansi48-81">  </span> <span class="ansi48-82">  </span> <span class="ansi48-83">  </span> <span class="ansi48-84">  </span> <span class="ansi48-85">  </span> <span class="ansi48-86">  </span> <span class="ansi48-87">  </span>

  88  <span class="ansi48-88">  </span> <span class="ansi48-89">  </span> <span class="ansi48-90">  </span> <span class="ansi48-91">  </span> <span class="ansi48-92">  </span> <span class="ansi48-93">  </span> <span class="ansi48-94">  </span> <span class="ansi48-95">  </span> <span class="ansi48-96">  </span> <span class="ansi48-97">  </span> <span class="ansi48-98">  </span> <span class="ansi48-99">  </span> <span class="ansi48-100">  </span> <span class="ansi48-101">  </span> <span class="ansi48-102">  </span> <span class="ansi48-103">  </span> <span class="ansi48-104">  </span> <span class="ansi48-105">  </span> <span class="ansi48-106">  </span> <span class="ansi48-107">  </span> <span class="ansi48-108">  </span> <span class="ansi48-109">  </span> <span class="ansi48-110">  </span> <span class="ansi48-111">  </span> <span class="ansi48-112">  </span> <span class="ansi48-113">  </span> <span class="ansi48-114">  </span> <span class="ansi48-115">  </span> <span class="ansi48-116">  </span> <span class="ansi48-117">  </span> <span class="ansi48-118">  </span> <span class="ansi48-119">  </span> <span class="ansi48-120">  </span> <span class="ansi48-121">  </span> <span class="ansi48-122">  </span> <span class="ansi48-123">  </span>

 124  <span class="ansi48-124">  </span> <span class="ansi48-125">  </span> <span class="ansi48-126">  </span> <span class="ansi48-127">  </span> <span class="ansi48-128">  </span> <span class="ansi48-129">  </span> <span class="ansi48-130">  </span> <span class="ansi48-131">  </span> <span class="ansi48-132">  </span> <span class="ansi48-133">  </span> <span class="ansi48-134">  </span> <span class="ansi48-135">  </span> <span class="ansi48-136">  </span> <span class="ansi48-137">  </span> <span class="ansi48-138">  </span> <span class="ansi48-139">  </span> <span class="ansi48-140">  </span> <span class="ansi48-141">  </span> <span class="ansi48-142">  </span> <span class="ansi48-143">  </span> <span class="ansi48-144">  </span> <span class="ansi48-145">  </span> <span class="ansi48-146">  </span> <span class="ansi48-147">  </span> <span class="ansi48-148">  </span> <span class="ansi48-149">  </span> <span class="ansi48-150">  </span> <span class="ansi48-151">  </span> <span class="ansi48-152">  </span> <span class="ansi48-153">  </span> <span class="ansi48-154">  </span> <span class="ansi48-155">  </span> <span class="ansi48-156">  </span> <span class="ansi48-157">  </span> <span class="ansi48-158">  </span> <span class="ansi48-159">  </span>

 160  <span class="ansi48-160">  </span> <span class="ansi48-161">  </span> <span class="ansi48-162">  </span> <span class="ansi48-163">  </span> <span class="ansi48-164">  </span> <span class="ansi48-165">  </span> <span class="ansi48-166">  </span> <span class="ansi48-167">  </span> <span class="ansi48-168">  </span> <span class="ansi48-169">  </span> <span class="ansi48-170">  </span> <span class="ansi48-171">  </span> <span class="ansi48-172">  </span> <span class="ansi48-173">  </span> <span class="ansi48-174">  </span> <span class="ansi48-175">  </span> <span class="ansi48-176">  </span> <span class="ansi48-177">  </span> <span class="ansi48-178">  </span> <span class="ansi48-179">  </span> <span class="ansi48-180">  </span> <span class="ansi48-181">  </span> <span class="ansi48-182">  </span> <span class="ansi48-183">  </span> <span class="ansi48-184">  </span> <span class="ansi48-185">  </span> <span class="ansi48-186">  </span> <span class="ansi48-187">  </span> <span class="ansi48-188">  </span> <span class="ansi48-189">  </span> <span class="ansi48-190">  </span> <span class="ansi48-191">  </span> <span class="ansi48-192">  </span> <span class="ansi48-193">  </span> <span class="ansi48-194">  </span> <span class="ansi48-195">  </span>

 196  <span class="ansi48-196">  </span> <span class="ansi48-197">  </span> <span class="ansi48-198">  </span> <span class="ansi48-199">  </span> <span class="ansi48-200">  </span> <span class="ansi48-201">  </span> <span class="ansi48-202">  </span> <span class="ansi48-203">  </span> <span class="ansi48-204">  </span> <span class="ansi48-205">  </span> <span class="ansi48-206">  </span> <span class="ansi48-207">  </span> <span class="ansi48-208">  </span> <span class="ansi48-209">  </span> <span class="ansi48-210">  </span> <span class="ansi48-211">  </span> <span class="ansi48-212">  </span> <span class="ansi48-213">  </span> <span class="ansi48-214">  </span> <span class="ansi48-215">  </span> <span class="ansi48-216">  </span> <span class="ansi48-217">  </span> <span class="ansi48-218">  </span> <span class="ansi48-219">  </span> <span class="ansi48-220">  </span> <span class="ansi48-221">  </span> <span class="ansi48-222">  </span> <span class="ansi48-223">  </span> <span class="ansi48-224">  </span> <span class="ansi48-225">  </span> <span class="ansi48-226">  </span> <span class="ansi48-227">  </span> <span class="ansi48-228">  </span> <span class="ansi48-229">  </span> <span class="ansi48-230">  </span> <span class="ansi48-231">  </span>

 232  <span class="ansi48-232">  </span> <span class="ansi48-233">  </span> <span class="ansi48-234">  </span> <span class="ansi48-235">  </span> <span class="ansi48-236">  </span> <span class="ansi48-237">  </span> <span class="ansi48-238">  </span> <span class="ansi48-239">  </span> <span class="ansi48-240">  </span> <span class="ansi48-241">  </span> <span class="ansi48-242">  </span> <span class="ansi48-243">  </span> <span class="ansi48-244">  </span> <span class="ansi48-245">  </span> <span class="ansi48-246">  </span> <span class="ansi48-247">  </span> <span class="ansi48-248">  </span> <span class="ansi48-249">  </span> <span class="ansi48-250">  </span> <span class="ansi48-251">  </span> <span class="ansi48-252">  </span> <span class="ansi48-253">  </span> <span class="ansi48-254">  </span> <span class="ansi48-255">  </span> <span class="ansi48-256">  </span> <span class="ansi48-257">  </span> <span class="ansi48-258">  </span> <span class="ansi48-259">  </span> <span class="ansi48-260">  </span> <span class="ansi48-261">  </span> <span class="ansi48-262">  </span> <span class="ansi48-263">  </span> <span class="ansi48-264">  </span> <span class="ansi48-265">  </span> <span class="ansi48-266">  </span> <span class="ansi48-267">  </span>
</pre>

The best way to understand is to try it out.

```bash
echo -e "testing \033[38;5;196;48;5;21mCOLOR1\033[38;5;208;48;5;159mCOLOR2\033[m"
```

### Reference Table

Once again, you most likely don't want to come back here just to look up a 256 color code. This is my own code. It's shared under [CC BY](http://creativecommons.org/licenses/by/3.0/).

```bash
#!/bin/bash
#
# generates an 8 bit color table (256 colors) for
# reference purposes, using the \033[48;5;${val}m
# ANSI CSI+SGR (see "ANSI Code" on Wikipedia)
#
echo -en "\n   +  "

for i in {0..35}; do
  printf "%2b " $i
done

printf "\n\n %3b  " 0
for i in {0..15}; do
  echo -en "\033[48;5;${i}m  \033[m "
done

#for i in 16 52 88 124 160 196 232; do
for i in {0..6}; do
  let "i = i*36 +16"
  printf "\n\n %3b  " $i
  for j in {0..35}; do
    let "val = i+j"
    echo -en "\033[48;5;${val}m  \033[m "
  done
done

echo -e "\n"
```

### See Also

- [Bash $PS1 Generator](http://www.kirsle.net/wizards/ps1.html)
- [Bash Shell PS1: 10 Examples to Make Your Linux Prompt like Angelina Jolie](http://www.thegeekstuff.com/2008/09/bash-shell-ps1-10-examples-to-make-your-linux-prompt-like-angelina-jolie/)
- [ArchWiki - Color Bash Prompt](https://wiki.archlinux.org/index.php/Color_Bash_Prompt)
- [8 Useful and Interesting Bash Prompts](http://maketecheasier.com/8-useful-and-interesting-bash-prompts/2009/09/04)
