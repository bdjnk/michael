+++
date = 2020-06-24T17:18:19Z
title = "How Leftism Won and Conservatism Can Resurge"

[taxonomies]
tag = ["politics"]
+++

[THESIS]: # "You need to make your principles and your values apparent in everything you do."
[LEFTISM]: # "The instinct to shift blame for any perceived problem to the political or social system."

The political left in America, and throughout much of the West, have won. And without a radical re-framing of conservative thinking, leftists will continue to win.

To stop losing, **conservatives must embody their principles and values as though their lives depend on it, and they must do it together**.

## The Current Reality

All of America's most culturally influential institutions are leftist. Conservatives fail to recognize that political victories are meaningless in this context.

### Conservative Denial

[Republicans increasingly feel like they’re ‘winning’ politically; Democrats feel the opposite](https://www.pewresearch.org/fact-tank/2020/02/26/republicans-increasingly-feel-like-theyre-winning-politically-democrats-feel-the-opposite/) (Pew Research Center, 26 Feb 2020). This is entirely fallacious, part illusion and part delusion.

The illusion is a mirage formed of leftist panic regarding Trump. These exhibitions of frantic agitation convince conservatives they must be winning.

The delusion is a misapprehension that the elections of politicians with modest conservative tendencies are triumphs. They are not, particularly since they operate in a system dominated by leftists, rendering any accomplishments temporary.

### Leftist Dominance

[These Charts Show The Political Bias Of Workers In Each Profession](https://www.businessinsider.com/charts-show-the-political-bias-of-each-profession-2014-11) (Business Insider, 03 Nov 2014). The charts came from Crowdpac, which at the time generated models for sorting political candidates using donation data.

As an interesting aside, Crowdpac, once a non-partisan organization, [currently](https://web.archive.org/web/20200625210600/https://www.crowdpac.com/) proudly proclaims "Crowdpac empowers *progressive* candidates and movements to *ignite change* in our democracy."

The following profession categories were *overwhelmingly leftist*, with nearly all donors at the left edge of the political spectrum:

- **Academics**  
  <small>[Homogenous: The Political Affiliations of Elite Liberal Arts College Faculty](https://www.nas.org/academic-questions/31/2/homogenous_the_political_affiliations_of_elite_liberal_arts_college_faculty) (National Association of Scholars, 29 Mar 2019) found an overall 12:1 ratio of Democrats to Republicans with "78 percent of the academic departments having either zero Republicans, or so few as to make no difference."</small>
- **Online Computer Services**  
  <small>According to GovPredict, from the mid-2000s to 2018, of money donated by employees of these companies the following percentages went to Democrats [Alphabet (Google) 90%](https://www.govpredict.com/blog/alphabets-political-contributions/), [Amazon 90%](https://www.govpredict.com/blog/contributions-by-amazon-employees/), [Apple 91%](https://www.govpredict.com/blog/contributions-by-apple-employees/), [Facebook 87%](https://www.govpredict.com/blog/political-contributions-by-facebook-employees/).</small>
- **Newspaper & Print Media**  
  <small>[A comparison to early coverage of past administrations](https://www.journalism.org/2017/10/02/a-comparison-to-early-coverage-of-past-administrations/) (Pew Research Center, 2 Oct 2017) found the first sixty days of media coverage received by Obama was 20% negative and 42% positive, while for Trump it was 62% negative and only 5% positive.</small>
- **Entertainment Industry**  
  <small>Statistics tracking the political messaging in music, movies, and television seemingly do not exist. Nonetheless, to acknowledge entertainment media is predominately left-leaning should be uncontroversial.</small>

These industries are culturally pivotal as they all curate information and attempt to control how people think and feel. They are the shapers of our societal ethos.

#### Impacts

[Exclusive poll: Young Americans are embracing socialism](https://www.axios.com/exclusive-poll-young-americans-embracing-socialism-b051907a-87a8-4f61-9e6e-0db75f7edc4a.html) (Axios, 10 Mar 2019). The data comes from The Harris Poll and shows a substantial increase in support for positions like government provided universal health-care and tuition-free college among Americans born after 1980 (Millennials and Generation Z).

[The Partisan Divide on Political Values Grows Even Wider](http://assets.pewresearch.org/wp-content/uploads/sites/5/2017/10/05162647/10-05-2017-Political-landscape-release.pdf) (Pew Research Center, 05 Oct 2017). The subtitle reads: "sharp shifts among Democrats on aid to needy, race, immigration".

This is a 100 page report, worth reading in full, mostly detailing the rapid and accelerating leftist capture of the Democrat party. It also breaks down responses by various factors, clearly demonstrating that young people, and people exposed to academia, are increasingly possessed by leftist values.

Here are the percentages of affirmative responses, broken down by age then academic attainment, for the question ***is it necessary to believe in God in order to be moral?***

<div id="god-age" class="chart"></div>
<div id="god-edu" class="chart"></div>

Here are the percentages of affirmative responses, broken down by age and academic attainment, for the question ***are stricter environmental laws and regulations worth the cost?***

<div id="env-age" class="chart"></div>
<div id="env-edu" class="chart"></div>

Here are the percentages of affirmative responses, broken down by age and academic attainment, for the question ***is racial discrimination the main reason many blacks can't get ahead?***

<div id="dis-age" class="chart"></div>
<div id="dis-edu" class="chart"></div>

[Views of racism as a major problem increase sharply, especially among Democrats](https://www.pewresearch.org/fact-tank/2017/08/29/views-of-racism-as-a-major-problem-increase-sharply-especially-among-democrats/) (Pew Research Center, 2 May 2019) provides percentages of agreement across several years for the statement ***racism is a big problem in our society today***.

<div id="racism" class="chart"></div>

This data reveals how leftism has won by capturing academia and the youth. Leftist perspectives are increasingly ubiquitous; they are the default mindset.

## The Leftist Mind

Leftists think in terms of systems. For a leftist everything is structural; we are all part of the structure, trapped within it, and unless we are actively breaking the system we are perpetuating it. Any perceived difficulties, no matter how personal, can ultimately be blamed on the political system and on those who ostensibly or allegedly control it.

### Ideological Roots

Though perhaps not the first source of leftist thinking, *The Discourse on the Origin of Inequality* by Jean-Jacques **Rousseau** is the vector of modern leftism. Therein he contends every society is inherently oppressive, being motivated by avarice, formed by deception, and sustained by injustice. 

> of society and of the laws, which increased the fetters of the weak, and the strength of the rich; irretrievably destroyed natural liberty, fixed for ever the laws of property and inequality; changed an artful usurpation into an irrevocable title; and for the benefit of a few ambitious individuals subjected the rest of mankind to perpetual labor, servitude, and misery.

This point of view, society seen as an oppressive structure perpetuated by "a few ambitious individuals", was fundamental in the French Revolution and the Reign of Terror. In that case it was the royals and their supporters.

In the Russian Revolution of 1905 it was the Tsars, and in the subsequent October Revolution it was the kulaks.

In Nazi Germany it was "the international bank Jews and their lackeys, the Democrats, Marxists, Jesuits, and Freemasons".

And so on, and so forth, the pattern remaining the same.

As a tragic addendum, it is unsurprising to find that "during Rousseau’s later years he was the victim of the delusion of persecution".

### Recent Manifestations

The anti-establishment sentiment of the 60s was cultural, but ultimately also political, spawning a bevy of movements casting various social ills as fundamentally a product of the political structures upheld by *The Establishment*.

In 1969 feminist Carol Hanisch published an essay entitled [The Personal is Political](http://www.carolhanisch.org/CHwritings/PIP.html). She contends the personal challenges women grapple with are actually political; the system is responsible.

> I’ve been forced to take off the rose colored glasses and face the awful truth about how grim my life really is as a woman.

> We came early to the conclusion that all alternatives are bad under present conditions.

The title is apt. To say the personal is political is to say politics is the root cause of your personal circumstances. Thus personal hardships are actually manifestations of political oppression, and you must alter the political structure as your only recourse.

This extends Marxist oppression theory beyond class and beyond labor to every aspect of being. It asserts a reality in which political dominance is a prerequisite to personal freedom, success, and happiness. In this view all challenges in life are dealt with through politics, and in the political arena you are fighting for your life.

## The Path Forward

Although outside of totalitarian states political structures are not broadly responsible for personal hardships, conservatives must recognize politics can and will be used by leftists to enforce policies they believe will ameliorate their personal struggles.

Indeed, leftists have sought to uproot, by any means, every social system they came to view as oppressive, from God to business to family to gender, while conservatives fought retreating defensive actions.

### Values Incarnate

Conservatives must make their principles and values apparent in everything they do. To speak and act as they believe without fear or hesitancy is the only moral mechanism for reducing leftist dominance.

At home, in school, at work, and in business, conservatives must not restrain themselves but instead represent what they believe in every statement and action.

#### Personally

I am an Orthodox Jew.

I include "thank God" in my responses to queries regarding my wellbeing. I append "God willing" to statements of intent I make. I attempt to speak clearly and effectively in support of morality and in opposition to its opposite.

I do not touch any woman who is not an immediate family member. This includes shaking hands, even in a business context. And similar to the *Pence Rule* (aka the *Modesto Manifesto*), which spawned a thousand angry articles, I do not seclude myself with any woman who is not an immediate family member. I have had the opportunity to explain to dozens of women that this is a matter of respect for the sanctity of sexuality and of sensitivity towards my wife.

Wherever I find myself, whether in the market checkout queue or in a work meeting, I discover opportunities to visibly represent my values.

#### Artifacts

Produce conservative artifacts in your domains of competence. 

Write poetry or prose, in fiction, whether comedy or tragedy, or in non-fiction, whether satire or serious. Publish in an article or a book, in a video or as audio. Call the world to hark to your voice.

Craft with your hands. Imbue physical material with moral meaning. You should knit, crochet, embroider, or bead; sculpt in wood, stone, metal, or glass; draw, paint, mosaic, or carve; all with intent to invest the values you hold in visible enduring form.

#### Internet

Share your works broadly across social media. Forgo anonymity and stand boldly, honest and unashamed. We have freedom of speech only so long as we maintain it, speaking as ourselves, forthrightly and without fear.

### Effective Associations

In *Thoughts on the Cause of the Present Discontents* Edmund **Burke** declares "the good must associate; else they will fall, one by one", presciently describing our current political climate.

> Whilst men are linked together, they easily and speedily communicate the alarm of any evil design. They are enabled to fathom it with common counsel, and to oppose it with united strength. Whereas, when they lie  dispersed, without concert, order, or discipline, communication is  uncertain, counsel difficult, and resistance impracticable. Where men are not acquainted with each other's principles, nor experienced in each other's talents, nor at all practised in their mutual habitudes and dispositions by joint efforts in business; no personal confidence, no friendship, no common interest, subsisting among them; it is evidently impossible that they can act a public part with uniformity, perseverance, or efficacy. In a connexion, the most inconsiderable man, by adding to the weight of the whole, has his value, and his use; out of it, the greatest talents are wholly unserviceable to the public. No man, who is not inflamed by vain-glory into enthusiasm, can flatter himself that his single, unsupported, desultory, unsystematic endeavours are of power to defeat the subtle designs and united Cabals of ambitious citizens. When bad men combine, the good must associate;  else they will fall, one by one, an unpitied sacrifice in a contemptible struggle.

The benefits of uniting and organizing are manifold, as Burke explicates above. Conservatives cannot be wary of joining together but must pursue association with others in opposition to the left as pivotal in switching from defense to offense.

#### Remote

Unfortunately, remote systems, like social media, are not conducive to forming durable associations, though they can be used to maintain associations and to coordinate.

Building the necessary trust can only happen in person.

#### Corporeal

Visit your neighbors. Invite them to a barbecue, a game night. Attend local events. Find those conservative souls, unhappy with leftist cultural dominance, and go gradually from being a scattering of individuals to being a group. Expand, and reach out to other groups. Lobby for conservative policy. Run for local political office.

This is how conservatives form effective associations of the sort Burke describes. This is how conservatives stop losing. It starts with **you**.

<style>
  .chart { min-height: 215px; }
</style>

<script src="https://cdn.jsdelivr.net/npm/apexcharts" defer></script>
<script>
window.addEventListener('load', event => {
  Apex = {
    colors: ['#1E1E1E'],
    chart: {
      type: 'line',
      height: '200',
      zoom: { enabled: false },
      toolbar: { show: false }
    },
    dataLabels: {
      enabled: true,
      style: { fontSize: '0.75em', fontWeight: 'normal' }
    },
    xaxis: {
      tooltip: { enabled: false },
      labels: {
        style: { fontSize: '0.8em' }
      }
    },
    yaxis: { show: false },
    tooltip: { enabled: false },
    grid: {
      xaxis: { lines: { show: true } },
      yaxis: { lines: { show: false } }
    }
  }
  const godAge = new ApexCharts(document.querySelector("#god-age"), {
    chart: {
      id: 'age',
      group: 'god',
    },
    series: [{
      name: '%',
      data: [57, 49, 38, 26]
    }],
    xaxis: {
      categories: ["65+", "64-50", "49-30", "29-18"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  const godEdu = new ApexCharts(document.querySelector("#god-edu"), {
    chart: {
      id: 'edu',
      group: 'god',
    },
    series: [{
      name: '%',
      data: ['56', '39', '30', '22']
    }],
    xaxis: {
      categories: ["Highschool", "Some College", "College Grad", "Postgrad"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  godAge.render();
  godEdu.render();
  const envAge = new ApexCharts(document.querySelector("#env-age"), {
    chart: {
      id: 'age',
      group: 'env',
    },
    series: [{
      name: '%',
      data: [50, 54, 62, 67]
    }],
    xaxis: {
      categories: ["65+", "64-50", "49-30", "29-18"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  const envEdu = new ApexCharts(document.querySelector("#env-edu"), {
    chart: {
      id: 'edu',
      group: 'env',
    },
    series: [{
      name: '%',
      data: [51, 59, 64, 75]
    }],
    xaxis: {
      categories: ["Highschool", "Some College", "College Grad", "Postgrad"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  envAge.render();
  envEdu.render();
  const disAge = new ApexCharts(document.querySelector("#dis-age"), {
    chart: {
      id: 'age',
      group: 'dis',
    },
    series: [{
      name: '%',
      data: [33, 34, 43, 54]
    }],
    xaxis: {
      categories: ["65+", "64-50", "49-30", "29-18"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  const disEdu = new ApexCharts(document.querySelector("#dis-edu"), {
    chart: {
      id: 'edu',
      group: 'dis',
    },
    series: [{
      name: '%',
      data: [34, 37, 49, 62]
    }],
    xaxis: {
      categories: ["Highschool", "Some College", "College Grad", "Postgrad"]
    },
    yaxis: { labels: { minWidth: 28 } }
  });
  disAge.render();
  disEdu.render();
  const racism = new ApexCharts(document.querySelector("#racism"), {
    colors: ['#67A4DE', '#1E1E1E'],
    chart: {},
    series: [
      { name: 'Democrats', data: [33, 42, 58, 76] },
      { name: 'Blacks', data: [44, 59, 73, 81] }
    ],
    xaxis: {
      type: 'datetime',
      categories: ['2009', '2010', '2015', '2017']
    }
  });
  racism.render();
});
</script>