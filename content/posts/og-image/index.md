+++
date = 2019-06-21T13:22:16-04:00
title = "Creating a Generic og:image Representing this Website"

[taxonomies]
tag = ["web"]
+++

In order to display optimally when shared to social media, every page of this site makes us of basic [Open Graph](http://ogp.me/) metadata, in particular the `og:image` property.

The image used is the following:

{{ img(path="posts/og-image/og.png") }}

That is a screenshot of the post [Introduction to Coding with SASS via Including ANSI Output in HTML](@/posts/ansi-scss.md#variables-and-lists) altered such that all words are replaced by boxes matching the color of the underlying text.

Want to see it in action? Try <a class="nox" href="javascript:(function() { var style = document.createElement('style'); style.type = 'text/css'; style.innerText = 'span.blankout { background-color: currentColor; }'; document.head.appendChild(style); fetch('https://raw.githubusercontent.com/padolsey/findAndReplaceDOMText/master/src/findAndReplaceDOMText.js').then(response => response.text()).then(code => { eval(code); Function(code)(); findAndReplaceDOMText(document.body, { find: /\S+/g, wrap: 'span', wrapClass: 'blankout' }); }); })();">clicking here</a>, or bookmark that link and click the bookmark on any page you want to completely redact.

How did I do this?

## findAndReplaceDOMText

First I wrapped each word in a span using [findAndReplaceDOMText](https://github.com/padolsey/findAndReplaceDOMText), a handy library for targeting the text in a DOM tree without concern for the DOM itself.

All I needed to do was run the following in the browser console ([Firefox](https://developer.mozilla.org/en-US/docs/Tools/Web_Console/The_command_line_interpreter), [Chrome](https://developers.google.com/web/tools/chrome-devtools/console/#javascript)).

```js
findAndReplaceDOMText(document.body, { find: /\S+/g, wrap: 'span', wrapClass: 'blankout' });
```

The above targets all sequences of non-whitespace characters and wraps them in a span with the class `blankout`.

## currentColor

Now I can target those spans with a bit of CSS using the little known [`currentColor` value](https://css-tricks.com/currentcolor/) to set the `background-color` to match the `color` property.

This is accomplished by adding the following via the browser CSS pane ([Firefox](https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Examine_and_edit_CSS#Add_rules), [Chrome](https://developers.google.com/web/tools/chrome-devtools/css/reference#style-rule)).

```css
span.blankout {
  background-color: currentColor;
}
```

That is all that's required, except...

## Screenshot

Several considerations arise when taking the screenshot, including the size and aspect ratio, the DPI, and the optimal compression.

### Size and Aspect Ratio

The optimal size and aspect ratio for open graph images is a matter of uncertainty. It differs between social media platforms and within the same platform by how it's viewed, and these standards change over time.

Without a marketing team devoted to optimizing per platform I made a best guess attempt, settling for the size 1500 x 750 which is a 2:1 aspect ratio.

Taking a screenshot of the correct size and aspect ratio is possible using *Device Mode* in Chrome, or *Responsive Design Mode* in Firefox (linked in the next section).

### DPR (Device Pixel Ratio)

With a Retina display, and possibly under other conditions, it's necessary to set your device pixel ratio (DPR) such that the screenshot is sized correctly.

This is can be done in either Firefox via [Responsive Design Mode](https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode#Device_selection) or Chrome via [Device Mode](https://developers.google.com/web/tools/chrome-devtools/device-mode/emulate-mobile-viewports#optional_controls_eg_touch_media_queries_dpr).

### oxipng

Once the screenshot exists, ensure it isn't any larger than strictly necessary. PNG compression can accomplishing this without degrading the quality. I recommend [oxipng](https://github.com/shssoichiro/oxipng).
