+++
date = 2018-10-25T16:45:10-04:00
title = "The Strong Message of Sensitivity in Parshas Yisro"

[taxonomies]
tag = ["torah"]
+++

Parsha Yisro has a strong message of sensitivity; an awareness and care of others.

## Perek 18

The parsha begins with Yisro, Moshe Rabbanu's father-in-law, traveling to meet the Jews in the desert.

### Yisro's Feelings

Posuk ט describes Yisro's feelings.

{% verse( text="וַיִּחַדְּ יִתְרוֹ עַל כָּל־הַטּוֹבָה אֲשֶׁר־עָשָׂה יְהוָה לְיִשְׂרָאֵל אֲשֶׁר הִצִּילוֹ מִיַּד מִצְרָיִם׃" ) %}
Yisro was happy about all the good that the Lord had done for Israel, that He had rescued them from the hands of the Egyptians.
{% end %}

However Rashi offers another explanation, from Gemara, Sanhedrin 94a.

{% verse( text="ויחד יתרו: וישמח יתרו, זהו פשוטו ומדרשו נעשה בשרו חדודין חדודין, מיצר על איבוד מצרים, היינו דאמרי אינשי גיורא עד עשרה דרי לא תבזי ארמאה באפיה׃" ) %}
*Yisro was happy:* and Yisro rejoiced. This is its simple meaning. However a midrash [explains that] his flesh became prickly (goosebumps) because he was aggrieved about the destruction of the Egyptians. This is [the source of] the popular saying: Do not disgrace a non-Jew in the presence of a convert, [even] up to the tenth generation [after the conversion].
{% end %}

This is a tremendous degree of sensitivity, and it's only the beginning.

### Yisro's Rebukes and Advice

In posuk יג it begins a description of the process of Torah adjudication Moshe engaged in with the Jewish people.

{% verse( text="וַיְהִי מִמָּחֳרָת וַיֵּשֶׁב מֹשֶׁה לִשְׁפֹּט אֶת־הָעָם וַיַּעֲמֹד הָעָם עַל־מֹשֶׁה מִן־הַבֹּקֶר עַד־הָעָרֶב׃" ) %}
It came about on the next day that Moshe sat down to judge the people, and the people stood before Moshe from the morning until the evening.
{% end %}

Rashi here quotes the Mechilta.

{% verse( text="וישב משה וגו' ויעמד העם: יושב כמלך וכולן עומדים, והוקשה הדבר ליתרו שהיה מזלזל בכבודן של ישראל והוכיחו על כך, שנאמר (פסוק יד) מדוע אתה יושב לבדך וכלם נצבים׃" ) %}
*that Moshe sat down…, and the people stood:* He sat like a king, and they [everyone who came to be judged] all stood. The matter displeased Yisro, that he [Moshe] belittled the respect due [the people of] Israel, and he reproved him about it, as it is said, in posuk יד “Why do you sit by yourself, and they are all standing?”
{% end %}

In Posuk יז Yisro further chastises Moshe, this time for the entire process.

{% verse( text="וַיֹּאמֶר חֹתֵן מֹשֶׁה אֵלָיו לֹא־טוֹב הַדָּבָר אֲשֶׁר אַתָּה עֹשֶׂה׃" ) %}
Moshe's father in law said to him, "The thing you are doing is not good."
{% end %}

At this point Rashi explains.

{% verse( text="ויאמר חתן משה: דרך כבוד קוראו הכתוב חותנו של מלך׃" ) %}
*Moshe's father-in-law said:* As a token of honor, Scripture refers to him as the king’s father-in-law [and not by his name].
{% end %}

Yisro had a tremendous sensitivity, toward both the Jewish people, and toward Moshe.

Crucially he does not just offer criticism, but instead offers a detailed plan for improving the situation, and is careful to preface that his plan should be run by G-d. As Rashi on posuk יט quoting the Mechilta says.

{% verse( text="איעצך ויהי א-להים עמך: בעצה, אמר לו צא המלך בגבורה׃" ) %}
*I will advise you, and may the Lord be with you:* in [this] counsel. He [Yisro] said to him [Moshe], “Go, consult the Lord [as to whether my advice is sound].”
{% end %}

## Perek 19

The parsha continues with preparations for the giving of the Torah.

### Women and Men

Upon Moshe's first ascent of the mountain G-d begins with a message to be conveyed verbatim to the Jewish people, starting with posuk ג.

{% verse( text="וּמֹשֶׁה עָלָה אֶל־הָאֱלֹהִים וַיִּקְרָא אֵלָיו יְהוָה מִן־הָהָר לֵאמֹר כֹּה תֹאמַר לְבֵית יַעֲקֹב וְתַגֵּיד לִבְנֵי יִשְׂרָאֵל׃" ) %}
Moshe ascended to G-d, and the Lord called to him from the mountain, saying, "So shall you say to the house of Jacob and tell the sons of Israel:"
{% end %}

Rashi relies again on the Mechilta to explain.

{% verse( text="כה תאמר: בלשון הזה וכסדר הזה׃" ) %}
*So shall you say:* With this language and in this order.
{% end %}

{% verse( text="לבית יעקב: אלו הנשים, תאמר להן בלשון רכה׃" ) %}
*to the house of Jacob:* These are the women. Say it to them in a gentle language.
{% end %}

{% verse( text="ותגיד לבני ישראל: עונשין ודקדוקין פרש לזכרים. דברים הקשין כגידין׃" ) %}
*and tell the sons of Israel:* The punishments and the details [of the laws] explain to the males, things that are as harsh as wormwood.
{% end %}

This is a sensitivity toward the different needs of women and men, to speak, even the exact same words, in the gender appropriate tone and manner.

And the words were the same, as Rashi at the conclusion of posuk ו says.

{% verse( text="אלה הדברים: לא פחות ולא יותר׃" ) %}
*These are the words:* No less and no more.
{% end %}

### Making Sure

Although Moshe had already warned the people to stay back, and set up boundaries, in posuk כא he is told to reinforce this warning.

{% verse( text="וַיֹּאמֶר יְהוָה אֶל־מֹשֶׁה רֵד הָעֵד בָּעָם פֶּן־יֶהֶרְסוּ אֶל־יְהוָה לִרְאוֹת וְנָפַל מִמֶּנּוּ רָב׃" ) %}
G-d said to Moshe, "Go down, warn the people lest they break [their formation to go nearer] to G-d, to see, and many of them will fall.
{% end %}

And Rashi bring the Mechilta who explains.

{% verse( text="ונפל ממנו רב: כל מה שיפול מהם ואפילו הוא יחידי חשוב לפני רב׃" ) %}
*and many of them will fall:* Whatever [number] falls from them, let it be even a single person, to Me it is considered [as if] many [have fallen].
{% end %}

In posuk כג Moshe argues.

{% verse( text="וַיֹּאמֶר מֹשֶׁה אֶל־יְהוָה לֹא־יוּכַל הָעָם לַעֲלֹת אֶל־הַר סִינָי כִּי־אַתָּה הַעֵדֹתָה בָּנוּ לֵאמֹר הַגְבֵּל אֶת־הָהָר וְקִדַּשְׁתּוֹ׃" ) %}
Moshe said to G-d, "The people cannot ascend to Mount Sinai, for You warned us saying, 'Set boundaries for the mountain and sanctify it.'"
{% end %}

And in posuk כד G-d insists.

{% verse( text="וַיֹּאמֶר אֵלָיו יְהוָה לֶךְ־רֵד" ) %}
But G-d said to him, "Go, descend ..."
{% end %}

Here Rashi explains this is a general principle.

{% verse( text="לך רד: והעד בהם שנית, שמזרזין את האדם קודם מעשה, וחוזרין ומזרזין אותו בשעת מעשה׃" ) %}
*Go, descend:* And warn them a second time. We admonish a person before the act [he is to perform], and we admonish him again at the time of the act [when it is to be performed].
{% end %}

This is the degree of sensitivity we must have to make sure others understand dangers and consequences.

## Conclusion

This parsha continues with the Ten Commandments.

All of this focus on sensitivity is the prelude to the greatest G-dly revelation and the fundamental transformation of the relationship between the spiritual and the material, between G-d and creation.

May it be a lesson for us.
