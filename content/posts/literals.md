+++
date = 2019-01-07T15:48:01-05:00
title = "Tagged Template Literals and Their Uses"

draft = true

[taxonomies]
tag = ["coding"]
+++

## Template Literals

These are fancy strings, demarcated by grave accent marks `` ` ``. They are "[strictly better strings](https://ponyfoo.com/articles/template-literals-strictly-better-strings)", allowing expression interpolation and automatic newline handing.

```js
const greeting = `Hello`;
const salutation = `${greeting}, template literals!`;
console.log(salutation); // outputs "Hello, template literals!"
```

### Tags

These are preprocessing functions prepended to template literals. So how do we do this, and why would we want to?

Let's say I wanted to transform all my numeric expressions into exponent notation.

```js
exp = (strings, ...keys) => {
	let s = '';
    keys.forEach((key, i) => {
			s += strings[i] + (isNaN(parseFloat(key)) ? key : key.toExponential());
    });
	return s + strings[strings.length-1];
}

exp`'Cause I'd get a ${1000} hugs  
From ${10000} lightning bugs`

// 'Cause I'd get a 1e+3 hugs  
// From 1e+4 lightning bugs
```

A tag function takes in a string literal, and can output... anything. This makes it very powerful.

## hyperHTML

This is a clever framework for generating `html` using tagged template literals.
