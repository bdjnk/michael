+++
date = 2018-09-18T11:56:24-04:00
title = "Linus Torvalds and the Linux Code of Conduct"

draft = true

[taxonomies]
tag = ["linux"]
+++

On Sunday, September 16th, 2018, Linus Trovalds sent an email entitled [*Linux 4.19-rc4 released, an apology, and a maintainership note*](https://lkml.org/lkml/2018/9/16/167).

The email breaks down into three sections.

- First, the normal release information and brief change-log.
- Second, a lengthy admission of personal introspection by Linus regarding alleged negative impacts of his uncivil behavior.
- Third, the new code of conduct, as Linus briefly mentions:

> The one change that stands out and merits mention is the code of conduct addition...

It's that third and final point I want to discuss, though I'll begin with the second point as a preface.

## Linus's Introspection

The introspective bulk of the email has two interleaved aspects, the personal and the organizational. While Linus doesn't distinguish between these, I will.

### His Admission

Discussions resulting from a scheduling mistake on his part led Linus to

> realizing that I really had been ignoring some fairly deep-seated feelings in the community.

After

> people in our community confronted me about my lifetime of not understanding emotions.

Leading him

> to the somewhat painful personal admission that hey, I need to change some of my behavior

Therefore he plans

> to take time off and get some assistance on how to understand people’s emotions and respond appropriately.

### My Reckoning

All of this seems perfectly reasonable from a personal perspective. We should all strive to be more cognizant of the emotional states of other people so as not distress them unnecessarily.

Linus is known for his biting dismissal of what he deems technically inferior. He is often brutal in his criticisms. By this mechanism he attempts to maintain the quality of the Linux kernel.

The questions is, what are Linus's obligations to the Linux kernel, and how aggressively must he cull incompetence and demand perfection to meet those obligations?

I don't know, but from the outside it seems Linus has done well. Linux is tremendously successful, partially because the kernel is excellent.

## The Code of Conduct Change

The release of *4.19-__rc4__* contained a commit titled "Code of Conduct: Let's revamp it." which removed the existing Code of Conflict and added a Code of Conduct.

Two brief asides.

First, that commit title is poorly crafted, violating nearly every convention. See [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) for a decent set of recommendations on this topic.

Second, this is a major culture change inserted into a release candidate. While it isn't a significant technical change, it is a change with significant ramifications for the future of the kernel.

The body of the commit message reads as follows.

> The Code of Conflict is not achieving its implicit goal of fostering civility and the spirit of 'be excellent to each other'.  Explicit guidelines have demonstrated success in other projects and other areas of the kernel.
>
> Here is a Code of Conduct statement for the wider kernel.  It is based on the Contributor Covenant as described at www.contributor-covenant.org
>
> From this point forward, we should abide by these rules in order to help make the kernel community a welcoming environment to participate in.

Here are questions which spring to mind.

- Did the Code of Conflict have an "implicit goal of fostering civility"?
- By what metric has success been demonstrated in other projects with explicit guidelines?
- What is this "Contributor Covenant" on which the new Code of Conduct is based?
  - Who wrote it and promotes it?
  - What purpose is it intended to serve?

### Heretofore, Code of Conflict

The [replaced Code of Conflict](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/code-of-conflict.rst?id=27c5a778dfe23d1de8d9ebfbc6a54595a79ac709) is worth quoting at length. It reads (almost in it's entirety) as follows.

> The Linux kernel development effort is a very personal process compared to "traditional" ways of developing software.  Your code and ideas behind it will be carefully reviewed, often resulting in critique and criticism.  The review will almost always require improvements to the code before it can be included in the kernel.  Know that this happens because everyone involved wants to see the best possible solution for the overall success of Linux.  This development process has been proven to create the most robust operating system kernel ever, and we do not want to do anything to cause the quality of submission and eventual result to ever decrease.
>
> If however, anyone feels personally abused, threatened, or otherwise uncomfortable due to this process, that is not acceptable.  If so, please contact the Linux Foundation's Technical Advisory Board [...]
>
> As a reviewer of code, please strive to keep things civil and focused on the technical issues involved.  We are all humans, and frustrations can be high on both sides of the process.  Try to keep in mind the immortal words of Bill and Ted, "Be excellent to each other."

Perusing the above Code of Conflict leads me to the understanding that civility is a secondary consideration.

The primary consideration is ensuring awareness and acceptance of the inevitable "critique and criticism" to maintain the "quality of submission" and continue the "success of Linux".

This seems to me a perfectly reasonable and sane approach to the development of deeply technical and complex software system which is a vital component of critical infrastructure.

### Henceforth, Code of Conduct

The [added Code of Conduct](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/code-of-conduct.rst?id=8a104f8b5867c682d994ffa7a74093c54469c11f) is an instantiation of a standard template called the [Contributor Covenant](https://www.contributor-covenant.org/).

This new Code of Conduct is a worrisome document, and I'll run through some of my concerns in the sections below.

#### Pledging the Impossible

The new Code of Conduct, taken literally, demands impossibilities. The pledge in the opening sentence is a particularly egregious example.

> contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone

This is clearly not something anyone can guarantee, how much more so "contributors and maintainers" whose job it is, not to police behavior and make unlikely promises, but rather, one would think, to contribute to and maintain the Linux kernel.

#### Divisive Attribute Enumeration

The sentence above should have ended with "everyone", but instead continues with the following lengthy enumeration of attributes.

> regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation

Any enumeration is a limiting rhetorical device, excluding whatever isn't included.

Further, this enumeration serves to split people, by driving a focus towards what divides us, rather than what unites us.

#### Unenforceable Unacceptable Behavior Rules

In addition to the impossible pledge mentioned above, specific explicitly included unacceptable behaviors are also unenforceable.

> Publishing others’ private information, such as a physical or electronic address, without explicit permission

> Other conduct which could reasonably be considered inappropriate in a professional setting

#### The Maintainer: Legislator, Judge, Jury, and Executioner

> Maintainers are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

> Maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.

#### Denouncing Heretic Maintainers

> Maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other members of the project’s leadership.

## Concerns Expressed on the Linux Mailing List

### Avoiding Politics

https://lkml.org/lkml/2018/9/25/620

### Punishing the Disabled

https://lkml.org/lkml/2018/9/19/234

## Supporters of the Code of Conduct

### Coraline Ada Ehmke

### Sage Sharp

https://twitter.com/_sagesharp_/status/1042769391279063040

## My Concerns, Summarized

The new Code of Conduct is vague where it should be specific, and specific where it should be universal.

presumption of guilt
