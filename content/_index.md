+++
title = "Michael Plotke"
description = "Porfolio of a senior software engineering with a track record of leading teams in building and deploying successful products."
template = "index.html"
+++

![headshot](me.png)

I'm a senior software engineer with a track record of leading teams in building and deploying successful products.

- [Resume PDF](Resume.pdf)
- [**Endorsements**](@/endorsements.md)

I wrote a [story of leadership and impact](@/experience.md) at Poll Everywhere, my most recent employer.

## Public Code

This [portfolio website](https://gitlab.com/bdjnk/michael) generated via the Zola static site engine and written in SCSS and vanilla JS.

A [Takahashi method inspired presentation application](https://github.com/bdjnk/show) written in Nim and using WebUI.

A [Gemini server](https://gitlab.com/bdjnk/leo) written in PHP.

A [website for my JavaScript Advent of Code solutions](https://gitlab.com/bdjnk/js-aoc) built on hyperHTML.

An [ambiance mod for Minetest](https://github.com/bdjnk/glow) written in Lua.

A [command line hangman game](https://github.com/bdjnk/hangman) written in C (and including a complete English word list).

## Tech Posts

[Introduction to C Programming - Coding a Command Line Hangman Game](@/posts/hangman.md), walking though the hangman game mentioned above.

[Building a Command Line Alarm Clock for Linux with Bash ](@/posts/alarm.md), which contains substantial information about shell scripting in general.

[Using ANSI Color Codes to Colorize Your Bash Prompt on Linux ](@/posts/prompt.md), which covers how prompts are constructed and how color works in terminals.

[Introduction to Coding with SASS via Including ANSI Output in HTML](@/posts/ansi-scss.md), where I explain how I easily included the colored terminal output in the above *prompt* article.

## Experiments

I currently use LEO, the Gemini server mentioned above, to serve a capsule from a 2008 Samsung NC10 netbook sitting in my basement running 32-bit Void Linux. You can find it at `gemini://plotke.me/` using a Gemini client, like [Lagrange](https://gmi.skyjake.fi/lagrange/).

Efficient [selection of cells in a massive table](https://stackblitz.com/edit/plate) with visualization using CSS Grid and various React performance optimization techniques.

Displaying [digital clock style numbers](https://codepen.io/bdjnk/pen/eYEjWpY) using React and SCSS.

Subtle [spinners](https://codepen.io/bdjnk/pen/RwNWEBv) using CSS animations.
